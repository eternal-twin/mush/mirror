Contents:

defs - contains sprite sheet definitions (sheet->src defines which image is associated with it)
images - images exported from client
mush_maps.json - data needed to draw a ship room


How to use it:
	- each room is created out of 4 layers 
	- each layer is a grid of 32 x 32 tiles
	- if a tile contains a value (not just spaces) do a lookup in defs for the value to find which object to draw

~~~

Assets originally exported by groomsh.
Things added by LAbare:
	- fixed a few defs files (see keys '_comment' and '_ignore');
	- extracted sprites from sheet_* files (# is for variants of a sprite, @ is for frames of an animated sprite), script runs with Python>=3.6.